from django.contrib.auth.models import User
from django.shortcuts import render, redirect
from idea_party.auth.forms import RegisterForm


def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid():
            User.objects.create_user(
                username=form.cleaned_data['username'],
                password=form.cleaned_data['password'],
                email=form.cleaned_data['email'],
            )
            return redirect('auth:login')
    else:
        form = RegisterForm()

    return render(request, 'registration/register.html', {'form': form})
