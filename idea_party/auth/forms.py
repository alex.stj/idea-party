from django import forms
from django.contrib.auth.models import User
from django.utils.translation import ugettext as _


class RegisterForm(forms.Form):
    username = forms.CharField(max_length=150, required=True)
    password = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField()

    def clean_username(self):
        username = self.cleaned_data['username']

        try:
            User.objects.get(username=username)
        except User.DoesNotExist:
            pass
        else:
            raise forms.ValidationError(_('This username is already registered.'))

        return username
